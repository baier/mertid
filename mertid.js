var moment = require('moment');
var holidays = require('date-holidays')

var hd = new holidays('NO') // opprett kalender for norske hellidager
var fs  = require("fs"); 
var args = require('yargs').argv; 

var now = moment().year();

var start_dato = moment(now+'-01-01');
var slutt_dato = moment(now+'-12-31');

var arbeidstimerPerUke = [];

// knytt ukenummer til dato
for (var m = moment(start_dato); m.diff(slutt_dato, 'days') <= 0; m.add(1, 'days')) {
    var isHoliday = hd.isHoliday(new Date(m.format('YYYY-MM-DD')));
      
    // hvis dagen er en helgedag, ikke legg til i liste med arbeidstimer
    if (m.day() != 6 && m.day() != 0 && isHoliday.type != 'public')  {
        
        arbeidstimerPerUke.push({
            ukenummer: m.week(),
            dato: m.format('YYYY-MM-DD')
        })
    }
}

var lambda = (o, ukedag) => {
    // hvis det ikke gis parameter, og ukenummeret <= parameteret som gis inn
    if(!(args.uke !== undefined && parseInt(ukedag.ukenummer) <= parseInt(args.uke))) {
        o[ukedag.ukenummer] = (o[ukedag.ukenummer] || 0) + 7.5;
    }
    return o;
}

var timerPerUke = arbeidstimerPerUke.reduce(lambda, {});

// les fil linje for linje
var faktiskeTimerJobbet = fs.readFileSync('./timeliste.txt').toString().split('\n');

var mertid = 0;

faktiskeTimerJobbet.forEach(function (value, i) {
    if (value != undefined && value!= 0 && timerPerUke[i+1] != undefined) {
        mertid = mertid + (value-timerPerUke[i+1]);   
        // fjern utkommentering under hvis du vil se mertid for hver dag
        console.log('%d: %d, %d, %d', i+1, value, timerPerUke[i+1], mertid);
    }
});

var timerForanBak = (mertid > 0) ? " foran" : " bak";
var str = `Du ligger ${mertid} timer`;
console.log(str + timerForanBak)
